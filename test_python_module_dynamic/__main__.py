import argparse
from os import environ
from typing import Tuple

parser = argparse.ArgumentParser()
parser.add_argument('-j', '--jobs')
parser.add_argument('-jn', '--job_name')
parser.add_argument('-o', '--output')


JOB_HEADER_TEMPLATE = """{name}:
  script:
"""


def get_templates(s: str) -> Tuple[str]:
    template_name_list = s.split(',')
    template_name_list = tuple(map(lambda x: x.strip(), template_name_list))

    template_script_list = []

    for name in template_name_list:
        try:
            template_script = environ[name]
            template_script_list.append(template_script)
        except KeyError:
            raise Exception(f"Шаблон с именем {name} не определен")

    return tuple(template_script_list)


def generate_job(job_name: str, instruction_list: Tuple[str], output_filename: str):
    text = JOB_HEADER_TEMPLATE.format(name=job_name)

    for inst in instruction_list:
        text += "    - " + inst + '\n'

    with open(output_filename, 'w+') as f:
        f.write(text)


if __name__ == '__main__':
    args = parser.parse_args()
    output_filename = args.output
    job_name = args.job_name
    jobs = args.jobs

    print(output_filename, job_name, jobs)

    if not jobs:
        raise Exception("Переменная --jobs не может быть пустой")

    if not job_name:
        raise Exception('Не определено название для job')

    if not output_filename:
        raise Exception('Не указано имя файла для записи')

    instructions = get_templates(jobs)
    generate_job(job_name, instruction_list=instructions, output_filename=output_filename)

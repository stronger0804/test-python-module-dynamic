import argparse
from typing import Any, Callable, Dict, List, Tuple

# import re

# from typing import Any, Callable, Generator, Iterable, Tuple

parser = argparse.ArgumentParser()
parser.add_argument('-o', '--output')
parser.add_argument('-j', '--jobs')


def get_generate_job(scripts: List[str]) -> str:
    job_template = """job1:\n\tscript:\n"""
    for script in scripts:
        job_template += '\t\t' + '- ' + script

    return job_template


def stop_and_remove_containers(*container_list: Tuple[str, ...]) -> str:
    instructions = []

    for container_name in container_list:
        instructions.append(f'docker stop {container_name} || true && docker rm {container_name} || true')

    return ';'.join(instructions)


TEMPLATE_LIBRARY: Dict[str, Callable] = {
    'stop_and_remove_containers': stop_and_remove_containers
}


def _parse_args(s: str):
    # выделить часть строки с аргументами
    start = s.find('(')
    args_string = s[start+1:-1]

    if not args_string:
        return tuple()

    args_names = args_string.split(',')
    args_names = map(lambda x: x.strip(), args_names)

    return tuple(args_names)


def get_func_name(s: str) -> str:
    end = s.find('(')
    name = s[:end]
    return name


def get_func_with_args(s: str) -> Tuple[Callable, Tuple[Any]]:
    func = TEMPLATE_LIBRARY.get(get_func_name(s))
    args = _parse_args(s)
    print(args)

    if not func:
        raise Exception('Шаблон не найден')

    return func, args


def make_yaml(filename: str, s: str):
    scripts = []
    func, args = get_func_with_args(s)
    script = func(*args)
    scripts.append(script)

    with open(filename, 'w+') as f:
        f.write(get_generate_job(scripts))


def run(args):
    if not args.output:
        raise Exception("Не указан файл")

    if not args.jobs:
        raise Exception("Не указан скрипт")

    make_yaml(args.output, args.jobs)


if __name__ == '__main__':
    args = parser.parse_args()
    run(args)

from typing import Tuple


def stop_and_remove_containers_by_project(project: str, container_list: Tuple[str, ...]) -> str:
    instructions = []
    if not project:
        raise Exception('Не указано имя проекта')

    if not container_list:
        raise Exception('Не указаны контейнеры')

    for container_name in container_list:
        full_container_name = f'{project}-{container_name}'
        instructions.append(f'docker stop {full_container_name} || true && docker rm {full_container_name} || true')

    return " ;\n".join(instructions)


def test() -> Tuple[str, ...]:
    return 'ls', "echo 'Hello'"
